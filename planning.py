import requests
from multiprocessing import Process, Queue

# The max ID to fetch. Anyone with an id greater than this will not be fetched.
MAX_ID = 500
MIN_ID = 160

# Replace the PHPSESSID by the one you get with an admin session on planning maker
PHPSESSID = 'PUT_YOUR_OWN'

COOKIES = {'PHPSESSID': PHPSESSID}

# Change this to the base URL of your planning maker
SERVER_URL = 'http://planning.gala.bde-insa-lyon.fr'

BASE_URL = SERVER_URL + '/orga/{}/'
CHECK_URL = BASE_URL + 'edit'
PLANNING_URL = BASE_URL + 'print'


def fetch_ids(queue, number_of_processes):
	"""
	Fetch the valid ids between 1 and MAX_ID.

	Put the valid ids in a message queue that will be read by fetch_plannings.
	"""
	for i in range(MIN_ID, MAX_ID):
		check = requests.head(CHECK_URL.format(i), cookies=COOKIES)
		if check.status_code < 400:
			print('found orga', i)
			queue.put(i)
		if i % 10 == 0:
			print(i)

	for _ in range(number_of_processes):
		queue.put(-1)


def fetch_plannings(queue,):
	"""
	Download the plannings from the valid ids in the message queue.
	"""
	print('starting process')
	i = queue.get()
	print('found id in queue', i)
	while i != -1:
		planning = requests.get(PLANNING_URL.format(i), cookies=COOKIES)
		with open('{}.pdf'.format(i), 'wb') as f:
			print('writing {}.pdf'.format(i))
			for chunk in planning:
				f.write(chunk)
		i = queue.get()
		print('found id in queue', i)


if __name__ == '__main__':
	if COOKIES.get('PHPSESSID') == 'PUT_YOUR_OWN':
		print('You must get an admin PHPSESSID from planning maker.')
		sys.exit(1)

	id_queue = Queue()
	number_of_processes = 2
	fetch_process = Process(target=fetch_ids, args=(id_queue, number_of_processes))
	fetch_process.start()
	processes = []

	for _ in range(number_of_processes):
		p = Process(target=fetch_plannings, args=(id_queue,))
		p.start()
		processes.append(p)

	fetch_process.join()

	for p in processes:
		p.join()

